#include <cstdlib>

#include <iostream>
#include <string>

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

using namespace boost::interprocess;

const char SHM_NAME[] = "test_shm";

// adapted from https://www.boost.org/doc/libs/1_54_0/doc/html/interprocess/sharedmemorybetweenprocesses.html
int main(int argc, char *argv[])
{
  if(argc < 2)
  {
    std::cout << "Shared memory writer" << std::endl;

    boost::interprocess::shared_memory_object shm(
        boost::interprocess::open_or_create,
        SHM_NAME, boost::interprocess::read_write);

    //Set size
    shm.truncate(1024);

    //Map the whole shared memory in this process
    boost::interprocess::mapped_region region(shm,
        boost::interprocess::read_write);

    //Write all the memory to 0x42
    memset(region.get_address(), 0x42, region.get_size());

    //Launch child process
    std::string s(argv[0]);
    s += " child ";
    std::cout << "Fork process " << s << std::endl;
    if(std::system(s.c_str()) != 0)
    {
      std::cout << "Child process failed" << std::endl;
      return EXIT_FAILURE;
    }
    std::cout << "Reader finished" << std::endl;
    boost::interprocess::shared_memory_object::remove(SHM_NAME);
  }
  else
  {
    std::cout << "Shared memory reader" << std::endl;

    //Open already created shared memory object.
    boost::interprocess::shared_memory_object shm(
        boost::interprocess::open_only,
        SHM_NAME,
        boost::interprocess::read_only);

    //Map the whole shared memory in this process
    boost::interprocess::mapped_region region(shm,
        boost::interprocess::read_only);

    //Check that memory was initialized to 1
    char *mem = static_cast<char*>(region.get_address());
    for(size_t i = 0; i < region.get_size(); i++)
    {
       if(mem[i] != 0x42)
       {
         std::cout << "Error checking memory" << std::endl;
         return EXIT_FAILURE;
       }
    }
    std::cout << "Shared memory OK" << std::endl;
  }

  return EXIT_SUCCESS;
}
