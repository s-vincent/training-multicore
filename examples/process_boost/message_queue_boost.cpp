#include <cstdlib>

#include <iostream>
#include <string>

#include <boost/interprocess/ipc/message_queue.hpp>

const char MQ_NAME[] = "test_mq";

int main(int argc, char *argv[])
{
  if(argc < 2)
  {
    char data[1024] = "Test MQ";

    //boost::interprocess::message_queue::remove(MQ_NAME);
    std::cout << "Message queue writer" << std::endl;

    boost::interprocess::message_queue mq(
        boost::interprocess::open_or_create,
        MQ_NAME,
        10, // max message number
        1024); // max size per message

    mq.send(data, sizeof(data), 0);
    std::cout << "Message sent!" << std::endl;

    //Launch child process
    std::string s(argv[0]);
    s += " child ";
    std::cout << "Fork process " << s << std::endl;
    if(std::system(s.c_str()) != 0)
    {
      std::cout << "Child process failed" << std::endl;
      return EXIT_FAILURE;
    }

    std::cout << "Reader finished" << std::endl;
    boost::interprocess::message_queue::remove(MQ_NAME);
  }
  else
  {
    char data[1024];
    boost::interprocess::message_queue::size_type recv_size = 0;
    unsigned int priority = 0;

    std::cout << "Message queue reader" << std::endl;

    boost::interprocess::message_queue mq(
        boost::interprocess::open_only,
        MQ_NAME);

    std::cout << "Message queue opened!" << std::endl;

    mq.receive(data, 1024, recv_size, priority);

    std::cout << std::string(data) << std::endl;

    std::cout << "Message queue OK" << std::endl;
  }

  return EXIT_SUCCESS;
}
