#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define FIFO_NAME "/tmp/test_fifo"

static void receiver(int fd, size_t data_size)
{
  char data[data_size];
  ssize_t len = -1;

  len = read(fd, data, data_size);

  if(len == -1)
  {
    perror("read");
    return;
  }

  data[len] = 0x00;
  fprintf(stdout, "Data received: %s\n", data);
  close(fd);
}

static void sender(int fd, size_t data_size)
{
  char data[data_size];

  strncpy(data, "Test", data_size);
  data[data_size - 1] = 0x00;

  if(write(fd, data, data_size) == -1)
  {
    perror("write");
  }
  close(fd);
}

int main(int argc, char** argv)
{
  int fd = -1;
  pid_t pid = -1;
  size_t data_size = 1024;

  if(mkfifo(FIFO_NAME, 0644) != 0)
  {
    perror("mkfifo");
    exit(EXIT_FAILURE);
  }

  pid = fork();
  switch(pid)
  {
  case -1:
    perror("fork");
    exit(EXIT_FAILURE);
    break;
  case 0:
    /* children */
    fd = open(FIFO_NAME, O_RDONLY);
    receiver(fd, data_size);
    close(fd);
    fprintf(stdout, "Child exiting\n");
    break;
  default:
    /* father */
    fd = open(FIFO_NAME, O_WRONLY);
    sender(fd, data_size);
    close(fd);

    /* wait children to avoid zombies */
    waitpid(pid, NULL, 0);

    /* remove fifo */
    unlink(FIFO_NAME);

    fprintf(stdout, "Father exiting\n");
    break;
  }

  return EXIT_SUCCESS;
}

