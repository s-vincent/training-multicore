#include <cstdlib>
#include <iostream>
#include <vector>

#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives.hpp>

template<typename T> uint64_t work(T nb, size_t size)
{
  uint64_t ret = 0;

  for(size_t i = 0 ; i < size ; i++)
  {
    ret += nb[i];
  }
  return ret;
}

int main(int argc, char** argv)
{
  boost::mpi::environment env(argc, argv);
  boost::mpi::communicator world;
  std::vector<uint64_t> nb;
  std::vector<uint64_t> sub_nb;
  uint64_t response = 0;
  int nb_size = 1000000;
  int sub_size = 0;
  int root = 0;
  uint64_t sum = 0;

  std::cout << "Hello world from processor " << world.rank() << " of "
    << world.size() << " processors" << std::endl;

  if(world.rank() == root)
  {
    nb.resize(nb_size);

    /* populate array with random int */
    for(int i = 0 ; i < nb_size; i++)
    {
      nb[i] = i;
   }
  }

  if(!(nb_size % world.size()))
  {
    sub_size = nb_size / world.size();
    sub_nb.resize(sub_size);

    /* distribute data to send to nodes */
    boost::mpi::scatter(world, nb, sub_nb.data(), sub_size, root);
  }
  else
  {
    int tmp = nb_size / world.size();
    int rem = nb_size % world.size();
    int shift = 0;
    std::vector<int> sendcounts;
    std::vector<int> displs;

    sendcounts.resize(world.size());
    displs.resize(world.size());

    /* populate array which will contains elements number and
     * offset for each processor
     */
    for(int i = 0 ; i < world.size() ; i++)
    {
      sendcounts[i] = tmp;
      if(rem)
      {
        sendcounts[i]++;
        rem--;
      }

      displs[i] = shift;
      shift += sendcounts[i];

      /*
      if(world.rank() == 0)
      {
        std::cout << "displs[" << i << "]=" << displs[i] << std::endl;
        std::cout << "sendcounts[" << i << "]=" << sendcounts[i] << std::endl;
      }
      */
    }

    // get number of element for the current process
    sub_size = sendcounts[world.rank()];
    sub_nb.resize(sub_size);

    // distinct code for root and non-root because of an assert in boost_mpi
    // https://github.com/boostorg/mpi/issues/138
    // same scatterv() call for all node should run fine on Boost >= 1.84.0
    if(world.rank() == root)
    {
      boost::mpi::scatterv(world, nb, sendcounts, displs, sub_nb.data(),
          sub_size, root);
    }
    else
    {
      boost::mpi::scatterv(world, nb, sub_nb.data(), sub_size, root);
    }
  }

  sum = work(sub_nb, sub_size);
  boost::mpi::reduce(world, sum, response,
      std::plus<uint64_t>(), root);

  if(world.rank() == root)
  {
    std::cout << "Sum is " << response << std::endl;

    /* verify answer by computing sequentially */
    std::cout << "Sequential sum: " << work(nb, nb.size()) << std::endl;;
  }

  return EXIT_SUCCESS;
}

