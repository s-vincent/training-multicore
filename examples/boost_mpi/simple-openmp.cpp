#include <cstdlib>
#include <iostream>

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

#if _OPENMP
#include <omp.h>
#endif

int main(int argc, char** argv)
{
#if _OPENMP
  boost::mpi::environment env(boost::mpi::threading::serialized);

  if(env.thread_level() < boost::mpi::threading::serialized)
  {
    env.abort(EXIT_FAILURE);
  }
#else
  boost::mpi::environment env;
#endif
  boost::mpi::communicator world;

  std::cout << "Hello world from processor " << world.rank() << " of "
    << world.size() << " processors" << std::endl;

  #pragma omp parallel
  {
    std::cout << "Process rank " << world.rank() << " thread "
      << omp_get_thread_num() << std::endl;
  }

  return EXIT_SUCCESS;
}

