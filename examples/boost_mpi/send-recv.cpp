#include <cstdlib>
#include <iostream>

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

int main(int argc, char** argv)
{
  boost::mpi::environment env;
  boost::mpi::communicator world;
  const int TAG_MSG = 42;

  std::cout << "Hello world from processor " << world.rank() << " of "
    << world.size() << " processors" << std::endl;

  if(world.rank() == 0)
  {
    std::vector<int> nb = {0, 1, 2, 3, 4};

    /* send message to every nodes except ourself (id 0) */
    for(int i = 1 ; i < world.size() ; i++)
    {
      world.send(i, TAG_MSG, nb);
    }

    std::cout << "Node " << world.rank() << " finished sending data"
      << std::endl;
  }
  else
  {
    std::vector<int> nb;
    int src = 0; /* receive from node 0 (master) */

    nb.resize(5);
    world.recv(src, TAG_MSG, nb);

    std::cout << "Node " << world.rank() << " received:";

    for(size_t i = 0 ; i < nb.size() ; i++)
    {
      std::cout << " " << nb[i];
    }
    std::cout << std::endl;
  }

  return EXIT_SUCCESS;
}

