#include <cstdlib>
#include <iostream>

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

int main(int argc, char** argv)
{
  boost::mpi::environment env;
  boost::mpi::communicator world;
  const int TAG_MSG = 42;

  std::cout << "Hello world from processor " << world.rank() << " of "
    << world.size() << " processors" << std::endl;

  if(world.rank() == 0)
  {
    std::vector<int> nb = {0, 1, 2, 3, 4};
    boost::mpi::request req;
    int do_work = 1;
    int dst = 1;

    req = world.isend(dst, TAG_MSG, nb);

    while(do_work)
    {
      // work
      // ...
      do_work = rand() % 2;
      std::cout << "Node " << world.rank() << " work=" << do_work << std::endl;

      std::cout << "Node " << world.rank() << " tests if request is sent"
        << std::endl;

      if(req.test())
      {
        break;
      }
    }

    /* no more work to do, if data are not yet sent, wait ! */
    if(!req.test())
    {
      req.wait();
    }

    std::cout << "Node " << world.rank() << " finished sending data"
      << std::endl;
  }
  else if(world.rank() == 1)
  {
    std::vector<int> nb;
    int src = 0; /* receive from node 0 (master) */
    boost::mpi::request req;

    nb.resize(5);
    req = world.irecv(src, TAG_MSG, nb);

    std::cout << "Node " << world.rank() << " waits for data" << std::endl;
    req.wait();

    std::cout << "Node " << world.rank() << " received:";

    for(size_t i = 0 ; i < nb.size() ; i++)
    {
      std::cout << " " << nb[i];
    }
    std::cout << std::endl;
  }

  return EXIT_SUCCESS;
}

