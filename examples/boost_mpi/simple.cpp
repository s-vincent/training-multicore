#include <cstdlib>
#include <iostream>

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

int main(int argc, char** argv)
{
  boost::mpi::environment env;
  boost::mpi::communicator world;

  std::cout << "Hello world from processor " << world.rank() << " of "
    << world.size() << " processors" << std::endl;

  return EXIT_SUCCESS;
}

