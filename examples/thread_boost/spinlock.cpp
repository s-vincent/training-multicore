#include <cstdlib>

#include <iostream>

#include <boost/thread/thread.hpp>
#include <boost/smart_ptr/detail/spinlock.hpp>

int a = 0;
boost::detail::spinlock spin;

void thread_func()
{
  std::cout << "Thread function" << std::endl;

  for(int i = 0 ; i < 1000000 ; i++)
  {
    spin.lock();
    a++;
    spin.unlock();
  }
}

int main(int argc, char** argv)
{
  boost::thread thread(thread_func);
  boost::thread thread2(thread_func);

  thread.join();
  thread2.join();

  std::cout << "a =" << a << std::endl;

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

