#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

void calc_init(int* a, int* b, int *c, size_t nb_elements)
{
  for(int i = 0 ; i < nb_elements ; i++)
  {
    a[i] = i + 1;
    b[i] = i + 1;
    c[i] = i + 1;
  }
}

void calc_notoptimized(int* a, int* b, int *c, size_t nb_elements)
{
  for(int i = 0 ; i < nb_elements ; i++)
  {
    a[i] += b[i];
    //a[0] += b[0]; // loop 1
    //a[1] += b[1]; // loop 2 b[1] is needed here (data deps), cannot vectorize
    //a[2] += b[2];
    //a[3] += b[3];

    b[i + 1] += c[i];
    //b[1] += c[0]; // loop 1 b[1] is calculated here
    //b[2] += c[1];
    //b[3] += c[2];
    //b[4] += c[3];
  }
}

void calc_optimized(int* a, int* b, int *c, size_t nb_elements)
{
  a[0] += b[0];
  for(int i = 0 ; i < nb_elements - 1; i++)
  {
    b[i + 1] += c[i];
    //b[1] += c[0]; // loop 1 b[1] is calculated here
    //b[2] += c[1]; // loop 2 b[2] is calculated here
    //b[3] += c[2]; // loop 3 b[3] is calculated here
    //b[4] += c[3]; // loop 4 b[4] is calculated here
    a[i + 1] += b[i + 1];
    //a[1] += b[1]; // loop 1 b[1] is needed here
    //a[2] += b[2]; // loop 2 b[2] is needed here
    //a[3] += b[3]; // loop 3 b[3] is needed here
    //a[4] += b[4]; // loop 4 b[4] is needed here
  }
  b[nb_elements - 1] += c[nb_elements - 1];
}

int main(int argc, char** argv)
{
  const size_t nb_elements = 100000000;
  int* a = NULL;
  int* b = NULL;
  int* c = NULL;
  double start = 0;
  double end = 0;

  a = malloc(nb_elements * sizeof(int));
  b = malloc(nb_elements * sizeof(int));
  c = malloc(nb_elements * sizeof(int));

  calc_init(a, b, c, nb_elements);

  fprintf(stdout, "No optmized:\n");
  start = omp_get_wtime();

  calc_notoptimized(a, b, c, nb_elements);

  end = omp_get_wtime();
  fprintf(stdout, "Time: %f ms\n", end - start);
  printf("%d %d %d\n", a[0], b[0], c[0]);

  // reinit to same value
  calc_init(a, b, c, nb_elements);
  fprintf(stdout, "Optmized:\n");
  start = omp_get_wtime();

  calc_optimized(a, b, c, nb_elements);

  end = omp_get_wtime();
  fprintf(stdout, "Time: %f ms\n", end - start);
  printf("%d %d %d\n", a[0], b[0], c[0]);


  free(a);
  free(b);
  free(c);
  return EXIT_SUCCESS;
}

