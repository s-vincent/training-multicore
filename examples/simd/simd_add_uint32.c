#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <time.h>

#include <xmmintrin.h>
#include <immintrin.h>

#define TAB_SIZE 1024000
#define INCR 8
#define INCR64 ((uint64_t)8 << 32 | 8)

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
  if((stop->tv_nsec - start->tv_nsec) < 0)
  {
    result->tv_sec = stop->tv_sec - start->tv_sec - 1;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
  }
  else
  {
    result->tv_sec = stop->tv_sec - start->tv_sec;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec;
  }
}


int main(int argc, char** argv)
{
  uint32_t tab[TAB_SIZE];
  struct timespec start;
  struct timespec stop;
  struct timespec result;

  /* sequential */
  for(size_t i = 0 ; i < TAB_SIZE ; i++)
  {
    tab[i] = i;
  }

  clock_gettime(CLOCK_REALTIME, &start);
  for(size_t i = 0 ; i < TAB_SIZE ; i++)
  {
    tab[i] = tab[i] + INCR;
  }
  clock_gettime(CLOCK_REALTIME, &stop);
  timespec_diff(&start, &stop, &result);
  printf("Execution time %ld.%09ld s (sequential)\n", result.tv_sec,
      result.tv_nsec);

  /* SSE */
  for(size_t i = 0 ; i < TAB_SIZE ; i++)
  {
    tab[i] = i;
  }

  clock_gettime(CLOCK_REALTIME, &start);
  __m128i ad = _mm_set_epi32(INCR, INCR, INCR, INCR);
  for(size_t i = 0 ; i < TAB_SIZE ; i+=4)
  {
    __m128i av = _mm_load_si128((__m128i*)(tab + i));
    __m128i c = _mm_add_epi32(av, ad);
    _mm_store_si128((__m128i*)(tab + i), c);
  }
  clock_gettime(CLOCK_REALTIME, &stop);
  timespec_diff(&start, &stop, &result);
  printf("Execution time %ld.%09ld s (SSE)\n", result.tv_sec,
      result.tv_nsec);

  /* AVX */
  for(size_t i = 0 ; i < TAB_SIZE ; i++)
  {
    tab[i] = i;
  }

  clock_gettime(CLOCK_REALTIME, &start);
  __m256i ad64 = _mm256_set_epi64x(INCR64, INCR64, INCR64, INCR64);
  for(size_t i = 0 ; i < TAB_SIZE ; i+=8)
  {
    __m256i av = _mm256_loadu_si256((__m256i*)(tab + i));
    __m256i c = _mm256_add_epi64(av, ad64);
    _mm256_storeu_si256((__m256i*)(tab + i), c);
  }
  clock_gettime(CLOCK_REALTIME, &stop);
  timespec_diff(&start, &stop, &result);
  printf("Execution time %ld.%09ld s (AVX)\n", result.tv_sec,
      result.tv_nsec);

  /*
  for(size_t i = 0 ; i < TAB_SIZE ; i++)
  {
    printf("%d\n", tab[i]);
  }
  */

  return EXIT_SUCCESS;
}

