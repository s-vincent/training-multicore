#include <cstdlib>

#include <iostream>

#include <sycl/sycl.hpp>
//#include <CL/sycl.hpp>

int main(int argc, char** argv)
{
  std::vector<int> tab1;
  std::vector<int> tab2;
  std::vector<int> result;
  size_t nb_elements = 1000000;

  tab1.resize(nb_elements);
  tab2.resize(nb_elements);
  result.resize(nb_elements);

  // initialization
  for(int i = 0 ; i < nb_elements ; i++)
  {
    tab1[i] = i;
    tab2[i] = i;
  }

  sycl::queue queue(sycl::default_selector_v);
  //sycl::queue queue(sycl::cpu_selector_v);
  //sycl::queue queue(sycl::gpu_selector_v);

  std::cout << "Device: " <<
    queue.get_device().get_info<sycl::info::device::name>() <<
    std::endl;

  // need braces to encapsulate buffers so their destructor can write data back!
  {
    // prepare buffers
    sycl::buffer<int, 1> d_tab1 { tab1.data(), sycl::range<1>(tab1.size()) };
    sycl::buffer<int, 1> d_tab2 { tab2.data(), sycl::range<1>(tab2.size()) };
    sycl::buffer<int, 1> d_result { result.data(), sycl::range<1>(result.size()) };

    queue.submit([&](sycl::handler& h) {
        auto data1 = d_tab1.get_access<sycl::access::mode::read_write>(h);
        auto data2 = d_tab2.get_access<sycl::access::mode::read_write>(h);
        auto dataresult = d_result.get_access<sycl::access::mode::read_write>(h);

        h.parallel_for<class sumvector>(sycl::range<1>{nb_elements}, [=](sycl::id<1> it) {
            const size_t i = it[0];
            dataresult[i] = data1[i] + data2[i];
            });
        });

    queue.wait();
  }

  // as buffers are destroyed, modified data is in result!
  std::cout << result[0] << " " << result[1] << " " << result[nb_elements - 2]
    << " " << result[nb_elements - 1] << std::endl;

  return EXIT_SUCCESS;
}

