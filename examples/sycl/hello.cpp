#include <cstdlib>

#include <iostream>

#include <sycl/sycl.hpp>

int main(int argc, char** argv)
{
  char result[32];

  sycl::queue queue(sycl::default_selector_v);
  //sycl::queue queue(sycl::cpu_selector_v);
  //sycl::queue queue(sycl::gpu_selector_v );

  std::cout << "Device: " <<
    queue.get_device().get_info<sycl::info::device::name>() <<
    std::endl;

  // need braces to encapsulate buffers so their destructor can write data back!
  {
    sycl::buffer<char, 1> d_result(result, sycl::range<1>(sizeof(result)));
    std::cout << sizeof(result) << std::endl;;

    queue.submit([&](sycl::handler& h) {
        auto data = d_result.get_access<sycl::access::mode::discard_write>(h);

        // sycl::range<1>{1} = 1 dimension with 1 value
        h.parallel_for<class hello>(sycl::range<1>{16}, [=](sycl::id<1> it) {
            const size_t i = it[0];
            data[i] = 'H';
            data[1] = 'e';
            data[2] = 'l';
            data[3] = 'l';
            data[4] = 'o';
            data[5] = ' ';
            data[6] = 'W';
            data[7] = 'o';
            data[8] = 'r';
            data[9] = 'l';
            data[10] = 'd';
            data[11] = '!';
            data[12] = '\0';
            });
        });
  }

  std::cout << "Wait" << std::endl;
  queue.wait();

  std::cout << result << std::endl;
  std::cout << "Finished" << std::endl;

  return EXIT_SUCCESS;
}
