#include <cstdlib>
#include <iostream>
#include <sycl/sycl.hpp>

int main(int argc, char** argv)
{
  for (const auto &dev : sycl::device::get_devices())
  {
    std::cout << dev.get_info<sycl::info::device::name>() << ": " << std::endl;

    try
    {
      const auto sgSizes = dev.get_info<sycl::info::device::sub_group_sizes>();
      std::cout << "\tGet sub_group_sizes: OK" << std::endl;
    }
    catch (std::exception)
    {
      std::cout << "\tGet sub_group_sizes: fail!" << std::endl;
    }

    try
    {
      sycl::queue q(dev);
      q.wait_and_throw();
      std::cout << "\tCreating queue successful!" << std::endl;
    }
    catch (std::exception)
    {
      std::cout << "\tCreating queue failed!" << std::endl;
    }
  }

  return EXIT_SUCCESS;
}
