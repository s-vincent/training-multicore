#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <omp.h>

int main(int argc, char** argv)
{
  //omp_set_nested(1);

  printf("%d\n", getpid());
  #pragma omp parallel
  {
    printf("rank %d\n", omp_get_thread_num());
    #pragma omp parallel
    {
      printf("\tSub-rank %d\n", omp_get_thread_num());
      sleep(60);
    }
  }

  return EXIT_SUCCESS;
}
