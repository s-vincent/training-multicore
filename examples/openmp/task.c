#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

void do_work(void)
{
  for(int i = 0 ; i < 1000; i++)
  {
    for(int j = 0 ; j < 1000; j++)
    {
    }
  }
}

int main (int argc, char *argv[])
{
  #pragma omp parallel num_threads(4)
  {
    printf("Call by thread %d\n", omp_get_thread_num());

    #pragma omp single
    {
      #pragma omp task
      {
        printf("Task A run by thread %d\n", omp_get_thread_num());
        do_work();
      }

      #pragma omp task
      {
        printf("Task B run by thread %d\n", omp_get_thread_num());
        do_work();
      }
    } /* implicit barrier */
    printf("Call by thread %d\n", omp_get_thread_num());
  }
  return EXIT_SUCCESS;
}

