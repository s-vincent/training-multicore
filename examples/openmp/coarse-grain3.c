#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  const int N = 65536;
  int* tab = malloc(sizeof(int) * N);

  for(int i = 0 ; i < N ; i++)
  {
    tab[i] = i;
  }

  double start = omp_get_wtime();

  #pragma omp parallel
  {
    int rank = omp_get_thread_num();
    int nb_threads = omp_get_num_threads();
    int deb = (rank * N) / nb_threads;
    int fin = ((rank + 1) * N) / nb_threads;

    //printf("deb=%d fin=%d\n", deb, fin);
    for(int j = 0 ; j < 1024 ; j++)
    for(int i = deb ; i < fin ; i++)
    {
      tab[i] = tab[i] + j;
    }
  }

  printf("%d %d %d %d\n", tab[0], tab[1], tab[2], tab[29999]);
  printf("End in %f\n", omp_get_wtime() - start);
  free(tab);
  return EXIT_SUCCESS;
}

