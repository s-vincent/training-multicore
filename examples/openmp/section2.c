#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  #pragma omp parallel sections
  {
    printf("Section A run by thread %d\n", omp_get_thread_num());

    #pragma omp section
    {
      printf("Section B run by thread %d\n", omp_get_thread_num());
    }

    #pragma omp section
    {
      printf("Section C run by thread %d\n", omp_get_thread_num());
    }
  }

  return EXIT_SUCCESS;
}

