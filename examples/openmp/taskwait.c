#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  #pragma omp parallel num_threads(4)
  {
    #pragma omp single
    {
      #pragma omp task
      {
        printf("Task A run by thread %d\n", omp_get_thread_num());

        #pragma omp task
        {
          printf("Task B run by thread %d\n", omp_get_thread_num());
          for(int i = 0 ; i < 500000 ; i++)
          {
            for(int j = 0 ; j < 10000 ; j++)
            {
            }
          }
          printf("Task B finished by thread %d\n", omp_get_thread_num());
        }

        for(int i = 0 ; i < 100000 ; i++)
        {
          for(int j = 0 ; j < 10000 ; j++)
          {
          }
        }

        #pragma omp taskwait
        {
          printf("Task A finished by thread %d\n", omp_get_thread_num());
        }
      }
    }
  }

  return EXIT_SUCCESS;
}

