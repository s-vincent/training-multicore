#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <omp.h>

int main(int argc, char** argv)
{
  const size_t N = 16384;
  int* tab = malloc(sizeof(int) * N);
  int* tab2 = malloc(sizeof(int) * N);
  double start = 0;
  uint64_t sum = 0;

  for(int i = 0 ; i < N ; i++)
  {
    tab[i] = i;
    tab2[i] = i;
  }

  start = omp_get_wtime();

  #pragma omp simd reduction(+:sum)
  //#pragma omp parallel for simd reduction(+:sum)
  for(int i = 0 ; i < N ; i++)
  {
    sum += tab[i] + tab2[i];
  }

  fprintf(stdout, "Calculation finished in %f s\n", omp_get_wtime() - start);
  fprintf(stdout, "Sum: %zu\n", sum);
  return EXIT_SUCCESS;
}
