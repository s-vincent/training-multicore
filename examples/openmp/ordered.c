#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  #pragma omp parallel for ordered
  for(int i = 0 ; i < 4 ; i++)
  {
    printf("Call by thread %d\n", omp_get_thread_num());

    #pragma omp ordered
    {
      printf("Section run by thread %d\n", omp_get_thread_num());
    }
    printf("End thread %d\n", omp_get_thread_num());
  }

  return EXIT_SUCCESS;
}

