#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  int myvar = 42;

  #pragma omp parallel firstprivate(myvar)
  {
    #pragma omp single copyprivate(myvar)
    {
      myvar = 452;
    } // barrier

    printf("[%d] myvar = %d\n", omp_get_thread_num(), myvar);
  }
  return EXIT_SUCCESS;
}

