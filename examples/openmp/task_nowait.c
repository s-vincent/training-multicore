#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  #pragma omp parallel num_threads(4)
  {
    #pragma omp single nowait
    {
      #pragma omp task
      {
        printf("Task A run by thread %d\n", omp_get_thread_num());
      }

      #pragma omp task
      {
        printf("Task B run by thread %d\n", omp_get_thread_num());
      }
    } /* no implicit barrier because of nowait */
    printf("Call by thread %d\n", omp_get_thread_num());
  }
  return EXIT_SUCCESS;
}

