#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

static int g_a = 42;
#pragma omp threadprivate(g_a)

int main(int argc, char** argv)
{
  g_a = 0;

  #pragma omp parallel copyin(g_a)
  //#pragma omp parallel
  {
    g_a += omp_get_thread_num();
    printf("[%d] %d\n", omp_get_thread_num(), g_a);
  }

  printf("End %d\n", g_a);

  return EXIT_SUCCESS;
}
