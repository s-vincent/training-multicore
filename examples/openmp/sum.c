#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main(int argc, char** argv)
{
  double start = 0;
  int* tab = NULL;
  long long sum = 0;
  long long loc_sum = 0;
  int nb_elements = 1000000;
  //int nb_elements = 1024 * 1024;
  //int nb_elements = 2048 * 2048;
  int nb_threads = 8;
  //int nb_threads = 1;

  // create big array
  tab = malloc(sizeof(int) * nb_elements);

  // populate array
  for(int i = 0 ; i < nb_elements ; i++)
  {
    tab[i] = i;
  }

  // start time
  start = omp_get_wtime();

  // create block on threads with a shared variable and private ones
  #pragma omp parallel num_threads(nb_threads) shared(sum, tab) private(loc_sum)
  {
    loc_sum = 0;

    // calculate sum of assigned array of numbers
    #pragma omp for
    for(int i = 0 ; i < nb_elements ; i++)
    {
      loc_sum += tab[i];
    }

    // be sure to protect shared variable with a lock
    #pragma omp atomic
    sum += loc_sum;
    fprintf(stdout, "%d -> %lld\n", omp_get_thread_num(), loc_sum);
  }

  fprintf(stdout, "Calculation finished in %f s\n", omp_get_wtime() - start);
  fprintf(stdout, "Sum is %lld\n", sum);

  free(tab);

  return EXIT_SUCCESS;
}
