#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  #pragma omp parallel
  {
    #pragma omp single
    {
      printf("Thread %d calculation\n", omp_get_thread_num());
      for(int i = 0 ; i < 500000 ; i++)
      {
        for(int j = 0 ; j < 10000 ; j++)
        {
        }
      }
      printf("Thread %d ends calculation\n", omp_get_thread_num());
    } /* implicit barrier */

    printf("Thread %d continue\n", omp_get_thread_num());
  }
  return EXIT_SUCCESS;
}

