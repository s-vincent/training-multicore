#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  const int N = 65536;
  int* tab = malloc(sizeof(int) * N);

  for(int i = 0 ; i < N ; i++)
  {
    tab[i] = i;
  }

  double start = omp_get_wtime();

  for(int j = 0 ; j < 1024 ; j++)
  {
    #pragma omp parallel for
    for(int i = 0 ; i < N ; i++)
    {
      tab[i] = tab[i] + i;
    }
  }

  printf("%d %d %d %d\n", tab[0], tab[1], tab[2], tab[3]);
  printf("End in %f\n", omp_get_wtime() - start);
  free(tab);

  return EXIT_SUCCESS;
}
