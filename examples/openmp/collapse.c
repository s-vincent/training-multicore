#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main(int argc, char** argv)
{
  #pragma omp parallel for collapse(2)
  for(int i = 0 ; i < 2 ; i++)
  {
    for(int j = 0 ; j < 32 ; j++)
    {
      printf("[%d]\n", omp_get_thread_num());
    }
  }
  printf("End\n");

  return EXIT_SUCCESS;
}
