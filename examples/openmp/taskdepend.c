#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

void do_work(void)
{
  for(int i = 0 ; i < 500000 ; i++)
  {
    for(int j = 0 ; j < 10000 ; j++)
    {
    }
  }
}

int main (int argc, char *argv[])
{
  int flag = 0;

  #pragma omp parallel num_threads(4) shared(flag)
  {
    #pragma omp single
    {
      #pragma omp task depend(out:flag)
      {
        printf("Task A run by thread %d\n", omp_get_thread_num());
        flag = 1;
        do_work();
      }

      #pragma omp task
      {
        printf("Task B run by thread %d\n", omp_get_thread_num());
        printf("Task B finished by thread %d\n", omp_get_thread_num());
      }

      #pragma omp task depend(in:flag)
      {
        printf("Task A finished by thread %d\n", omp_get_thread_num());
        printf("Task C flag %d\n", flag);
      }
    }
  }

  return EXIT_SUCCESS;
}

