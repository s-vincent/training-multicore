#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>
#include <omp.h>

int main(int argc, char** argv)
{
  int iteration = 0;

  #pragma omp parallel shared(iteration)
  {
    for(int i = 0 ; i < 4 ; i++)
    {
      //printf("id %d - %d\n", omp_get_thread_num(), i);
      printf("wait for single\n");
      if(omp_get_thread_num() == 0 ||
          omp_get_thread_num() ==7)
      {
        sleep(1);
      }
      #pragma omp single
      {
        iteration++;
      }

      if(iteration >= 3)
      {
        printf("exit\n");
        pthread_exit(NULL);
      }
    }
  }

  printf("%d\n", iteration);
  return EXIT_SUCCESS;
}

