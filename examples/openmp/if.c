#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

void work(size_t nb_data)
{
  #pragma omp parallel for if(nb_data > 8)
  for(int i = 0 ; i < nb_data ; i++)
  {
      printf("Hello World from thread %d\n", omp_get_thread_num());
  }
}

int main (int argc, char *argv[])
{
  printf("nb_data = 4\n");
  work(4);

  printf("nb_data = 16\n");
  work(16);
  return EXIT_SUCCESS;
}

