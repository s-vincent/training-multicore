#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

int main (int argc, char *argv[])
{
  int flag = 0;

  if(omp_get_cancellation() == 0)
  {
    fprintf(stdout, "Run with OMP_CANCELLATION=true environment variable!\n");
    exit(EXIT_FAILURE);
  }

  #pragma omp parallel
  {
    #pragma omp for
    for(int i = 0 ; i < 1000000 ; i++)
    {
      if(i == 42)
      {
        printf("Set flag\n");
        #pragma omp atomic write
        flag = 1;
        #pragma omp cancel for
      }
      else if(i == 900000)
      {
        printf("942 reached!\n");
        #pragma omp atomic write
        flag = 942;
      }
      #pragma omp cancellation point for
    }
  }

  printf("Flag: %d\n", flag);
  return EXIT_SUCCESS;
}

