#include <cstdlib>

#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx;
std::mutex mtx2;

void thread_func()
{
  std::this_thread::sleep_for(std::chrono::microseconds(10));

  {
    std::scoped_lock lock(mtx, mtx2);
    std::this_thread::sleep_for(std::chrono::microseconds(10));
  }
}

void thread_func2()
{
  std::this_thread::sleep_for(std::chrono::microseconds(10));

  {
    std::scoped_lock lock(mtx2, mtx);
    std::this_thread::sleep_for(std::chrono::microseconds(10));
  }
}


int main(int argc, char** argv)
{
  std::thread thread(thread_func);
  std::thread thread2(thread_func2);

  thread.join();
  thread2.join();

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

