#include <cstdlib>

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>


void thread_func(std::condition_variable& cond, std::mutex& mtx, bool& good_a)
{
  int a = 0;

  for(int i = 0 ; i < 1000000 ; i++)
  {
    a++;

    if(a == 1000000)
    {
      std::unique_lock<std::mutex> lock(mtx);
      good_a = true;
      cond.notify_all();
    }
  }
}

int main(int argc, char** argv)
{
  bool good_a = false;
  std::mutex mtx;
  std::condition_variable cond;
  std::thread th(thread_func, std::ref(cond), std::ref(mtx), std::ref(good_a));

  {
    std::unique_lock<std::mutex> lock(mtx);
    while(!good_a)
    {
      std::cout << "Wait for good_a to be 1" << std::endl;
      cond.wait(lock);
    }
  }

  std::cout << "a = " << good_a << std::endl;

  /* wait threads to terminate */
  th.join();

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

