#include <cstdlib>

#include <iostream>
#include <thread>
#include <future>

void myfunc(std::promise<int>&& promise)
{
  std::cout << "Thread sleep for 2 seconds" << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(2));
  promise.set_value(42);
}

int main(int argc, char** argv)
{
  std::promise<int> promise;
  std::future<int> future = promise.get_future();
  std::thread th(myfunc, std::move(promise));

  int val = future.get(); // blocking call
  std::cout << "Value " << val << std::endl;

  th.join();

  return EXIT_SUCCESS;
}
