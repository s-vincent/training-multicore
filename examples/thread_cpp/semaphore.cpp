#include <cstdlib>

#include <iostream>
#include <thread>
#include <array>

#include <semaphore>

void thread_func(int i, std::counting_semaphore<>& sem)
{
  sem.acquire();
  std::cout << "Hello from thread " << i << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(2));
  sem.release();
}

int main(int argc, char** argv)
{
  std::array<std::thread, 10> th;
  // 2 tokens
  std::counting_semaphore<> sem(2);
  size_t nb = th.size();

  for(size_t i = 0 ; i < nb ; i++)
  {
    th[i] = std::thread(thread_func, i, std::ref(sem));
  }

  for(size_t i = 0 ; i < nb ; i++)
  {
    th[i].join();
  }
  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

