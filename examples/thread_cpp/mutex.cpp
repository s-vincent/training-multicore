#include <cstdlib>

#include <iostream>
#include <thread>

void thread_func(int& a)
{
  std::cout << "Thread function" << std::endl;

  for(int i = 0 ; i < 1000000 ; i++)
  {
    a++;
  }
}

int main(int argc, char** argv)
{
  int a = 0;
  std::thread thread(thread_func, std::ref(a));
  std::thread thread2(thread_func, std::ref(a));

  thread.join();
  thread2.join();

  std::cout << "a = " << a << std::endl;

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

