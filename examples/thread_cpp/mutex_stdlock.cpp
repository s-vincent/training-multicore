#include <cstdlib>

#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx;
std::mutex mtx2;

void thread_func()
{
  std::this_thread::sleep_for(std::chrono::microseconds(10));

  {
    //defer_lock does NOT lock the mutex
    std::unique_lock<std::mutex> locka(mtx, std::defer_lock);
    std::unique_lock<std::mutex> lockb(mtx2 ,std::defer_lock);
    std::lock(locka, lockb);

    std::this_thread::sleep_for(std::chrono::microseconds(10));
  }
}

void thread_func2()
{
  std::this_thread::sleep_for(std::chrono::microseconds(10));

  {
    //defer_lock does NOT lock the mutex
    std::unique_lock<std::mutex> locka(mtx2 ,std::defer_lock);
    std::unique_lock<std::mutex> lockb(mtx, std::defer_lock);
    std::lock(locka, lockb);

    std::this_thread::sleep_for(std::chrono::microseconds(10));
  }
}


int main(int argc, char** argv)
{
  std::thread thread(thread_func);
  std::thread thread2(thread_func2);

  thread.join();
  thread2.join();

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

