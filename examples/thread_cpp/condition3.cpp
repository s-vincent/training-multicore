#include <cstdlib>
#include <iostream>
#include <chrono>
#include <queue>

#include <thread>
#include <mutex>
#include <condition_variable>

static void func(std::mutex& mtx, std::condition_variable& cnd,
    std::queue<int>& queue)
{
  std::cout << "Thread created" << std::endl;
  int value = 0;

  {
    std::unique_lock<std::mutex> lock(mtx);
    while(queue.size() == 0)
    {
      cnd.wait(lock); // mtx is unlocked
    }

    // get value
    value = queue.front();
    queue.pop();
  } // mtx is unlocked

  // processing outside lock
  std::cout << "Result " << value  << std::endl;
  // code
}

int main(int argc, char** argv)
{
  std::mutex mtx;
  std::condition_variable cnd;
  std::queue<int> queue;

  std::thread th(func, std::ref(mtx), std::ref(cnd),
      std::ref(queue));

  std::this_thread::sleep_for(std::chrono::seconds(5));

  // notify
  {
    std::unique_lock<std::mutex> lock(mtx);
    cnd.notify_all();
    queue.push(42);
  } // unlock

  th.join();
  return EXIT_SUCCESS;
}

