#include <cstdlib>

#include <iostream>
#include <array>
#include <thread>
#include <barrier>

void thread_func(int arg, int& a, std::barrier<>& barrier)
{
  int idx = arg;
  std::cout << "Thread " << idx << " starts" << std::endl;

  for(int i = 0 ; i < 10000 ; i++)
  {
    for(int j = 0 ; j < 10000 ; j++)
    {
      a++;
    }
  }

  std::cout << "Thread " << idx << " waits" << std::endl;
  barrier.arrive_and_wait();
}

int main(int argc, char** argv)
{
  std::array<std::thread, 10> th;
  size_t nb = th.size();
  int a = 0;
  std::barrier<> barrier(11);

  for(size_t i = 0 ; i < nb ; i++)
  {
    th[i] = std::thread(thread_func, i, std::ref(a), std::ref(barrier));
  }

  barrier.arrive_and_wait();
  std::cout << "All threads are initialized" << std::endl;

  for(size_t i = 0 ; i < nb ; i++)
  {
    /* wait threads to terminate */
    th[i].join();
  }

  std::cout << "a = ";
  std::cout << a << std::endl;

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

