#include <cstdlib>

#include <iostream>
#include <thread>
#include <future>

int myfunc()
{
  std::cout << "Thread " << std::this_thread::get_id() <<
    " sleep for 2 seconds" << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(2));
  return 42;
}

int main(int argc, char** argv)
{
  std::cout << "Main " << std::this_thread::get_id() << std::endl;

  std::future<int> future = std::async(&myfunc);
  int val = future.get(); // blocking call

  std::cout << "Value " << val << std::endl;

  return EXIT_SUCCESS;
}
