#include <cstdlib>

#include <iostream>
#include <thread>
#include <mutex>

int a = 0;

void thread_func(std::mutex& mtx)
{
//  std::cout << "Thread function" << std::endl;

  for(int i = 0 ; i < 1000000 ; i++)
  {
    mtx.lock();
    a++;
    mtx.unlock();
  }
}

int main(int argc, char** argv)
{
  std::mutex mtx;
  std::thread thread(thread_func, std::ref(mtx));
  std::thread thread2(thread_func, std::ref(mtx));

  thread.join();
  thread2.join();

  std::cout << a << std::endl;

  std::cout << "Exit program" << std::endl;
  return EXIT_SUCCESS;
}

