#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <CL/cl.h>

static char* opencl_get_program(const char* file_path, size_t* program_size)
{
  char* ret = NULL;
  size_t file_size = 0;
  FILE* f = fopen(file_path, "r");
  size_t n = 0;

  assert(f);

  /* get the size of the content */
  fseek(f, 0, SEEK_END);
  file_size = ftell(f);
  rewind(f);

  ret = (char*)malloc(sizeof(char) * (file_size + 1));

  if(!ret)
  {
    fclose(f);
    return ret;
  }

  n = fread(ret, sizeof(char), file_size, f);
  ret[n] = 0x00;

  if(n != file_size)
  {
    free(ret);
    ret = NULL;
  }

  if(program_size)
  {
    *program_size = file_size;
  }
  fclose(f);
  return ret;
}

int main(int argc, char** argv)
{
  cl_platform_id* platforms = NULL;
  char platform_name[1024];
  cl_uint nb_platforms = 0;
  cl_device_type device_type = CL_DEVICE_TYPE_ALL;
  char* program_data = NULL;
  size_t program_size = 0;
  cl_int status = -1;

  /* get OpenCL compute program source */
  program_data = opencl_get_program("hello.cl", &program_size);
  if(program_data == NULL)
  {
    fprintf(stderr, "Failed to get OpenCL program source code\n");
    exit(EXIT_FAILURE);
  }

  /* use first platform available */
  if(clGetPlatformIDs(0, NULL, &nb_platforms) == CL_SUCCESS)
  {
    status = 0;
    if(nb_platforms > 0)
    {
      platforms = malloc(sizeof(cl_platform_id) * nb_platforms);

      status = clGetPlatformIDs(nb_platforms, platforms, NULL);
      if(status != CL_SUCCESS)
      {
        fprintf(stderr, "Failed to retrieves platform (%d)\n", status);
        status = -1;
      }
    }
    else
    {
      fprintf(stdout, "No OpenCL platform found\n");
      status = -1;
    }
  }

  if(status != CL_SUCCESS)
  {
    free(platforms);
    free(program_data);
    exit(EXIT_FAILURE);
  }

  fprintf(stdout, "Found %d platform(s)\n", nb_platforms);

  for(size_t pi = 0 ; pi < nb_platforms ; pi++)
  {
    cl_platform_id platform = platforms[pi];
    cl_device_id* devices = NULL;
    cl_uint nb_devices = 0;
    cl_context context = NULL;
    cl_program program = NULL;
    char buf[2048];
    cl_uint refcount = 0;

    clGetPlatformInfo(platform, CL_PLATFORM_NAME, sizeof(platform_name),
        platform_name, NULL);

    fprintf(stdout, "Platform: %s\n", platform_name);

    /* platform profile info */
    clGetPlatformInfo(platform, CL_PLATFORM_PROFILE, sizeof(buf),
        buf, NULL);
    fprintf(stdout, "\tCL_PLATFORM_PROFILE: %s\n", buf);

    /* platform version info */
    clGetPlatformInfo(platform, CL_PLATFORM_VERSION, sizeof(buf),
        buf, NULL);
    fprintf(stdout, "\tCL_PLATFORM_VERSION: %s\n", buf);

    /* platform name */
    clGetPlatformInfo(platform, CL_PLATFORM_NAME, sizeof(buf),
        buf, NULL);
    fprintf(stdout, "\tCL_PLATFORM_NAME: %s\n", buf);

    /* platform vendor */
    clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, sizeof(buf),
        buf, NULL);
    fprintf(stdout, "\tCL_PLATFORM_VENDOR: %s\n", buf);

    /* platform extensions */
    clGetPlatformInfo(platform, CL_PLATFORM_EXTENSIONS, sizeof(buf),
        buf, NULL);
    fprintf(stdout, "\tCL_PLATFORM_EXTENSIONS: %s\n", buf);
    fprintf(stdout, "\n");

    if(clGetDeviceIDs(platform, device_type, 0, NULL, &nb_devices)
        == CL_SUCCESS)
    {
      if(nb_devices > 0)
      {
        devices = malloc(sizeof(cl_device_id) * nb_devices);

        status = clGetDeviceIDs(platform, device_type, nb_devices, devices,
            NULL);
        if(status != CL_SUCCESS)
        {
          fprintf(stderr, "Failed to get devices for %s (%d)\n",
              platform_name, status);
          status = -1;
        }
      }
      else
      {
        fprintf(stdout, "No devices found for platform %s\n", platform_name);
        status = -1;
      }
    }

    if(status != CL_SUCCESS)
    {
      free(devices);
      continue;
    }

    /* create context associated with devices */
    context = clCreateContext(NULL, nb_devices, devices, NULL, NULL, &status);

    if(status != CL_SUCCESS)
    {
      fprintf(stderr, "Failed to create context for platform %s (%d)\n",
          platform_name, status);
      free(devices);
      continue;
    }

    /* show info of the context */
    clGetContextInfo(context, CL_CONTEXT_REFERENCE_COUNT, sizeof(cl_uint),
        &refcount, NULL);
    fprintf(stdout, "\tCL_CONTEXT_REFERENCE_COUNT: %u\n", refcount);

    /* create and build OpenCL code */
    program = clCreateProgramWithSource(context, 1, (const char**)&program_data,
        &program_size, &status);

    if(status != CL_SUCCESS)
    {
      fprintf(stderr, "Failed to create program for platform %s (%d)\n",
          platform_name, status);
      clReleaseContext(context);
      free(devices);
      continue;
    }

    clGetProgramInfo(program, CL_PROGRAM_SOURCE, sizeof(buf), buf, NULL);
    fprintf(stdout, "%s\n", buf);

    status = clBuildProgram(program, nb_devices, devices, NULL, NULL, NULL);
    if(status != CL_SUCCESS)
    {
      size_t log_size = 0;
      char* log = NULL;
      fprintf(stderr, "Error compiling OpenCL program (%d)\n", status);

      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL,
          &log_size);

      log = malloc(sizeof(char) * (log_size + 1));
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size,
          log, NULL);
      fprintf(stderr, "Log:\n%s\n", log);
      free(log);
      clReleaseContext(context);
      free(devices);
      continue;
    }

    fprintf(stdout, "\tFound %d device(s)\n", nb_devices);
    for(size_t di = 0 ; di < nb_devices ; di++)
    {
      cl_device_id device = devices[di];
      char device_name[1024];
      cl_kernel* kernels = NULL;
      cl_uint nb_kernels = 0;
      cl_kernel kernel = NULL;
      cl_command_queue queue = NULL;
      cl_mem buffer = NULL;
      size_t global_size[] = { 1 };
      size_t local_size[] = { 1 };
      cl_bool available = 0;
      cl_uint address_bits = 0;
      cl_ulong mem_size = 0;

      clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_name), device_name,
          NULL);

      fprintf(stdout, "\tDevice: %s\n", device_name);

      clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(buf), buf, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_NAME: %s\n", buf);

      clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(buf), buf, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_VENDOR: %s\n", buf);

      clGetDeviceInfo(device, CL_DEVICE_VERSION, sizeof(buf), buf, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_VERSION: %s\n", buf);

      clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type),
          &device_type, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_TYPE: %ld\n", device_type);

      clGetDeviceInfo(device, CL_DEVICE_AVAILABLE, sizeof(cl_bool),
          &available, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_AVAILABLE: %d\n", available);

      clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(cl_uint),
          &address_bits, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_ADDRESS_BITS: %d\n", address_bits);

      clGetDeviceInfo(device, CL_DEVICE_EXTENSIONS, sizeof(buf), buf, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_EXTENSIONS: %s\n", buf);

      clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong),
          &mem_size, NULL);
      fprintf(stdout, "\t\tCL_DEVICE_GLOBAL_MEM_SIZE: %ld\n", mem_size);

      clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(buf), buf, NULL);
      fprintf(stdout, "\t\tCL_DRIVER_VERSION: %s\n", buf);
      fprintf(stdout, "\n");

      status = clCreateKernelsInProgram(program, 0, NULL, &nb_kernels);
      if(status == CL_SUCCESS)
      {
        kernels = malloc(sizeof(cl_kernel) * nb_kernels);
        clCreateKernelsInProgram(program, nb_kernels, kernels, NULL);
      }
      else
      {
        fprintf(stderr, "Failed to retrieve kernel (%d)\n", status);
        continue;
      }

     fprintf(stdout, "\tKernel(s) available:\n");
     for(cl_uint ki = 0 ; ki < nb_kernels ; ki++)
     {
       cl_uint num_args = 0;
       clGetKernelInfo(kernels[ki], CL_KERNEL_FUNCTION_NAME, sizeof(buf), buf,
           NULL);
       clGetKernelInfo(kernels[ki], CL_KERNEL_NUM_ARGS, sizeof(cl_uint),
           &num_args, NULL);

       fprintf(stdout, "\t\tCL_KERNEL_FUNCTION_NAME: %s ", buf);
       fprintf(stdout, "\t\tCL_KERNEL_NUM_ARGS: %u\n", num_args);

       if(!strcmp(buf, "blank"))
       {
         kernel = kernels[ki];
       }
     }

#if CL_TARGET_OPENCL_VERSION < 200
      queue = clCreateCommandQueue(context, device, 0, &status);
#else
      queue = clCreateCommandQueueWithProperties(context, device, NULL,
          &status);
#endif

     buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, 1024 * sizeof(char),
         NULL, &status);

      status = clEnqueueNDRangeKernel(queue, kernel,
          1, /* dimension */
          NULL, global_size, local_size, /* offset, global, local */
          0, NULL, NULL); /* events_nb, event_wait, event */

      if(status != CL_SUCCESS)
      {
        fprintf(stderr, "\t\tFailed to enqueue kernel (%d)\n", status);
      }
      else
      {
        fprintf(stdout, "\t\tEnqueue task success\n");
      }

      clFlush(queue);
      clFinish(queue);
      clReleaseCommandQueue(queue);

      for(cl_uint ki = 0 ; ki < nb_kernels ; ki++)
      {
        clReleaseKernel(kernels[ki]);
      }
      free(kernels);

      clReleaseMemObject(buffer);
    }

    clReleaseProgram(program);
    clReleaseContext(context);
    free(devices);
  }

  free(platforms);
  free(program_data);

  return EXIT_SUCCESS;
}

