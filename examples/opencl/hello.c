#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <CL/cl.h>

static char* opencl_get_program(const char* file_path, size_t* program_size)
{
  char* ret = NULL;
  size_t file_size = 0;
  FILE* f = fopen(file_path, "r");
  size_t n = 0;

  assert(f);

  /* get the size of the content */
  fseek(f, 0, SEEK_END);
  file_size = ftell(f);
  rewind(f);

  ret = (char*)malloc(sizeof(char) * (file_size + 1));

  if(!ret)
  {
    fclose(f);
    return ret;
  }

  n = fread(ret, sizeof(char), file_size, f);
  ret[n] = 0x00;

  if(n != file_size)
  {
    free(ret);
    ret = NULL;
  }

  if(program_size)
  {
    *program_size = file_size;
  }
  fclose(f);
  return ret;
}

int main(int argc, char** argv)
{
  cl_platform_id* platforms = NULL;
  char platform_name[1024];
  cl_uint nb_platforms = 0;
  cl_device_type device_type = CL_DEVICE_TYPE_ALL;
  char* program_data = NULL;
  size_t program_size = 0;
  cl_int status = -1;

  /* get OpenCL compute program source */
  program_data = opencl_get_program("hello.cl", &program_size);
  if(program_data == NULL)
  {
    fprintf(stderr, "Failed to get OpenCL program source code\n");
    exit(EXIT_FAILURE);
  }

  /* use first platform available */
  if(clGetPlatformIDs(0, NULL, &nb_platforms) == CL_SUCCESS)
  {
    status = 0;
    if(nb_platforms > 0)
    {
      platforms = malloc(sizeof(cl_platform_id) * nb_platforms);

      status = clGetPlatformIDs(nb_platforms, platforms, NULL);
      if(status != CL_SUCCESS)
      {
        fprintf(stderr, "Failed to retrieves platform (%d)\n", status);
        status = -1;
      }
    }
    else
    {
      fprintf(stdout, "No OpenCL platform found\n");
      status = -1;
    }
  }

  if(status != CL_SUCCESS)
  {
    free(platforms);
    free(program_data);
    exit(EXIT_FAILURE);
  }

  for(size_t pi = 0 ; pi < nb_platforms ; pi++)
  {
    cl_platform_id platform = platforms[pi];
    cl_device_id* devices = NULL;
    cl_uint nb_devices = 0;
    cl_context context = NULL;
    cl_program program = NULL;

    clGetPlatformInfo(platform, CL_PLATFORM_NAME, sizeof(platform_name),
        platform_name, NULL);

    fprintf(stdout, "Platform: %s\n", platform_name);

    if(clGetDeviceIDs(platform, device_type, 0, NULL, &nb_devices)
        == CL_SUCCESS)
    {
      if(nb_devices > 0)
      {
        devices = malloc(sizeof(cl_device_id) * nb_devices);

        status = clGetDeviceIDs(platform, device_type, nb_devices, devices,
            NULL);
        if(status != CL_SUCCESS)
        {
          fprintf(stderr, "Failed to get devices for %s (%d)\n",
              platform_name, status);
          status = -1;
        }
      }
      else
      {
        fprintf(stdout, "No devices found for platform %s\n", platform_name);
        status = -1;
      }
    }

    if(status != CL_SUCCESS)
    {
      free(devices);
      continue;
    }

    /* create context associated with devices */
    context = clCreateContext(NULL, nb_devices, devices, NULL, NULL, &status);

    if(status != CL_SUCCESS)
    {
      fprintf(stderr, "Failed to create context for platform %s (%d)\n",
          platform_name, status);
      free(devices);
      continue;
    }

    /* create and build OpenCL code */
    program = clCreateProgramWithSource(context, 1, (const char**)&program_data,
        &program_size, &status);

    if(status != CL_SUCCESS)
    {
      fprintf(stderr, "Failed to create program for platform %s (%d)\n",
          platform_name, status);
      clReleaseContext(context);
      free(devices);
      continue;
    }

    status = clBuildProgram(program, nb_devices, devices, NULL, NULL, NULL);
    if(status != CL_SUCCESS)
    {
      size_t log_size = 0;
      char* log = NULL;
      fprintf(stderr, "Error compiling OpenCL program (%d)\n", status);

      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL,
          &log_size);

      log = malloc(sizeof(char) * (log_size + 1));
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size,
          log, NULL);
      fprintf(stderr, "Log:\n%s\n", log);

      free(log);
      clReleaseContext(context);
      free(devices);
      continue;
    }

    for(size_t di = 0 ; di < nb_devices ; di++)
    {
      cl_device_id device = devices[di];
      char device_name[1024];
      cl_kernel kernel = NULL;
      char kernel_name[] = "hello";
      cl_command_queue queue = NULL;
      cl_mem buffer = NULL;
      size_t global_size[] = { 1 };
      size_t local_size[] = { 1 };

      clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_name), device_name,
          NULL);

      fprintf(stdout, "\tDevice: %s\n", device_name);

      kernel = clCreateKernel(program, "hello", &status);
      if(status != CL_SUCCESS)
      {
        fprintf(stderr, "\tFailed to retrieve kernel %s\n", kernel_name);
        continue;
      }

#if CL_TARGET_OPENCL_VERSION < 200
      queue = clCreateCommandQueue(context, device, 0, &status);
#else
      queue = clCreateCommandQueueWithProperties(context, device, NULL,
          &status);
#endif

      buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, 1024 * sizeof(char),
          NULL, &status);

      status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&buffer);
      status |= clEnqueueNDRangeKernel(queue, kernel,
          1, /* dimension */
          NULL, global_size, local_size, /* offset, global, local */
          0, NULL, NULL); /* events_nb, event_wait, event */

      if(status != CL_SUCCESS)
      {
        fprintf(stderr, "\tFailed to enqueue kernel (%d)\n", status);
      }
      else
      {
        char str[1024];

        /* read output result from OpenCL kernel */
        status = clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0,
            sizeof(str) * sizeof(char), str, 0, NULL, NULL);
        fprintf(stdout, "\tResult: %s\n", str);
      }

      clFlush(queue);
      clFinish(queue);
      clReleaseCommandQueue(queue);
      clReleaseKernel(kernel);
      clReleaseMemObject(buffer);
    }

    clReleaseProgram(program);
    clReleaseContext(context);
    free(devices);
  }

  free(platforms);
  free(program_data);

  return EXIT_SUCCESS;
}

