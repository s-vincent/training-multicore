#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <CL/opencl.hpp>

int main(int argc, char** argv)
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  std::ifstream file;
  std::stringstream program_data;

  file.open("hello.cl");
  if(!file.is_open())
  {
    std::cout << "Failed to open hello.cl" << std::endl;
    exit(EXIT_FAILURE);
  }

  program_data << file.rdbuf();

  std::cout << "Found " << platforms.size() << " platform(s)" << std::endl;

  if(platforms.size() == 0)
  {
    std::cout << "No platform available!" << std::endl;
    exit(EXIT_FAILURE);
  }

  for(cl::Platform platform : platforms)
  {
    std::vector<cl::Device> devices;

    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    std::cout << "Platform: " << platform.getInfo<CL_PLATFORM_NAME>()
      << std::endl;
    std::cout << "\tCL_PLATFORM_PROFILE: " <<
      platform.getInfo<CL_PLATFORM_PROFILE>() << std::endl;
    std::cout << "\tCL_PLATFORM_VERSION: " <<
      platform.getInfo<CL_PLATFORM_VERSION>() << std::endl;
    std::cout << "\tCL_PLATFORM_VENDOR: " <<
      platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl;
    std::cout << "\tCL_PLATFORM_EXTENSIONS: " <<
      platform.getInfo<CL_PLATFORM_EXTENSIONS>() << std::endl;

    std::cout << std::endl;

    cl::Context context(devices);
    cl::Program::Sources sources;

//    std::cout << "\tCL_CONTEXT_DEVICES: " <<
//      context.getInfo<CL_CONTEXT_DEVICES>() << std::endl;
    std::cout << "\tCL_CONTEXT_REFERENCE_COUNT: " <<
      context.getInfo<CL_CONTEXT_REFERENCE_COUNT>() << std::endl;

    sources.push_back(program_data.str());

    cl::Program program(context, sources);
    if(program.build(devices) != CL_SUCCESS)
    {
      std::cout << "Failed to build program" << std::endl;
      std::cout << "Log" << std::endl <<
        program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl;;
      break;
    }

    std::cout << "\tCL_PROGRAM_SOURCE: " << std::endl <<
      program.getInfo<CL_PROGRAM_SOURCE>() << std::endl;


    std::cout << "\tFound " << devices.size() << " device(s)" << std::endl;
    for(cl::Device device : devices)
    {
      std::vector<cl::Kernel> kernels;
      cl::Kernel blank;
      cl::CommandQueue queue(context, device);
      cl::Buffer buffer(context, CL_MEM_READ_WRITE, sizeof(char) * 1024);

      std::cout << "\t\tDevice: " << device.getInfo<CL_DEVICE_NAME>()
        << std::endl;
      std::cout << "\t\tCL_DEVICE_VENDOR: "
        << device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
      std::cout << "\t\tCL_DEVICE_VERSION: "
        << device.getInfo<CL_DEVICE_VERSION>() << std::endl;
      std::cout << "\t\tCL_DEVICE_TYPE: "
        << device.getInfo<CL_DEVICE_TYPE>() << std::endl;
      std::cout << "\t\tCL_DEVICE_AVAILABLE: "
        << device.getInfo<CL_DEVICE_AVAILABLE>() << std::endl;
      std::cout << "\t\tCL_DEVICE_ADDRESS_BITS: "
        << device.getInfo<CL_DEVICE_ADDRESS_BITS>() << std::endl;
      std::cout << "\t\tCL_DEVICE_EXTENSIONS: "
        << device.getInfo<CL_DEVICE_EXTENSIONS>() << std::endl;
      std::cout << "\t\tCL_DEVICE_GLOBAL_MEM_SIZE: "
        << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() << std::endl;
      std::cout << "\t\tCL_DRIVER_VERSION: "
        << device.getInfo<CL_DRIVER_VERSION>() << std::endl;
      std::cout << std::endl;

      program.createKernels(&kernels);

      std::cout << "\tKernel(s) available:" << std::endl;
      for(cl::Kernel kernel : kernels)
      {
        std::cout << "\t\tCL_KERNEL_FUNCTION_NAME: " <<
          kernel.getInfo<CL_KERNEL_FUNCTION_NAME>() << std::endl;
        std::cout << "\t\tCL_KERNEL_NUM_ARGS: " <<
          kernel.getInfo<CL_KERNEL_NUM_ARGS>() << std::endl;

        if(kernel.getInfo<CL_KERNEL_FUNCTION_NAME>() == "blank")
        {
           blank = kernel;
        }
      }

      blank.setArg(0, buffer);

      queue.enqueueNDRangeKernel(blank, cl::NullRange, cl::NDRange(1),
          cl::NDRange(1));

      std::cout << "\t\tEnqueue task success" << std::endl;
    }

    std::cout << std::endl;
  }

  std::cout << "End" << std::endl;

  return EXIT_SUCCESS;
}

