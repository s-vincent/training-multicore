#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <CL/opencl.hpp>

int main(int argc, char** argv)
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  std::ifstream file;
  std::stringstream program_data;

  file.open("hello.cl");
  if(!file.is_open())
  {
    std::cout << "Failed to open hello.cl" << std::endl;
    exit(EXIT_FAILURE);
  }

  program_data << file.rdbuf();

  if(platforms.size() == 0)
  {
    std::cout << "No platform available!" << std::endl;
    exit(EXIT_FAILURE);
  }

  for(cl::Platform platform : platforms)
  {
    std::vector<cl::Device> devices;
    std::string kernel_name = "hello";

    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    std::cout << "Platform: " << platform.getInfo<CL_PLATFORM_NAME>()
      << std::endl;

    cl::Context context(devices);
    cl::Program::Sources sources;

    //std::cout << program_data.str() << std::endl;
    sources.push_back(program_data.str());

    cl::Program program(context, sources);
    if(program.build(devices) != CL_SUCCESS)
    {
      std::cout << "Failed to build program" << std::endl;
      std::cout << "Log" << std::endl <<
        program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl;;
      break;
    }

    for(cl::Device device : devices)
    {
      cl::Kernel kernel = cl::Kernel(program, kernel_name.c_str());
      cl::CommandQueue queue(context, device);
      cl::Buffer buffer(context, CL_MEM_READ_WRITE, sizeof(char) * 1024);

      std::cout << "\tDevice: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

      kernel.setArg(0, buffer);

      queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1),
          cl::NDRange(1));

      char str[1024];
      queue.enqueueReadBuffer(buffer, CL_TRUE, 0, sizeof(char) * 1024, str);

      std::cout << "\tResult: " << str << std::endl;
    }
  }

  return EXIT_SUCCESS;
}

