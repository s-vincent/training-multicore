#!/bin/sh

HOMEUSER=""
REALUSER=""

if [ $(id -u) -ne 0 ]
then
  echo "Script has to be run as root or sudo"
  exit 1
fi

if [ ! -z $SUDO_USER -a "$SUDO_USER" != "root" ]; then
 HOMEUSER=/home/$SUDO_USER
 REALUSER=$SUDO_USER
else
 HOMEUSER=/root
 REALUSER=root
fi

if [ -f /etc/redhat-release ]; then
  echo "Setup RedHat GNU/Linux for multicore training session"

  yum install yum-utils
  dnf config-manager --set-enabled crb
  yum install -y @"Development Tools"
  yum install -y epel-release
  yum install -y gcc-c++ libgomp
  yum install -y clang libomp
  yum install -y gdb valgrind strace ltrace git
  yum install -y openmpi openmpi-devel boost-openmpi-devel
  yum install -y ocl-icd ocl-icd-devel opencl-headers clinfo

  if [ ! -f /usr/include/CL/opencl.hpp ]; then
    ln -sf cl2.hpp /usr/include/CL/opencl.hpp
  fi

tee > /etc/yum.repos.d/oneAPI.repo << EOF
[oneAPI]
name=Intel® oneAPI repository
baseurl=https://yum.repos.intel.com/oneapi
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
EOF

  yum install -y intel-oneapi-runtime-opencl

  cat <<EOF >> $HOMEUSER/.bashrc
module load mpi
export OMPI_MCA_pml=ucx
EOF

elif [ -f /etc/debian_version ]; then
  echo "Setup Debian GNU/Linux for multicore training session"

  apt-get update -y
  apt-get install -y build-essential g++ libgomp1
  apt-get install -y clang libomp5
  apt-get install -y gdb valgrind strace ltrace git
  apt-get install -y openmpi-bin libopenmpi-dev openmpi-doc libboost-mpi-dev
  apt-get install -y ocl-icd-opencl-dev opencl-headers clinfo
  apt-get install -y gnupg2

  wget -O- https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB \
    | gpg --dearmor | tee /usr/share/keyrings/oneapi-archive-keyring.gpg > /dev/null
  tee > /etc/apt/sources.list.d/oneAPI.list << EOF
deb [signed-by=/usr/share/keyrings/oneapi-archive-keyring.gpg] https://apt.repos.intel.com/oneapi all main
EOF

  apt-get update
  apt-get install -y intel-oneapi-runtime-opencl

else
  echo "Error: distribution not supported!"
  echo "This script supports only Debian-based and RedHat-based distributions"

  exit 1
fi

git clone --depth 1 https://framagit.org/s-vincent/training-multicore.git $HOMEUSER/training-multicore
chown -R $REALUSER:$REALUSER $HOMEUSER/training-multicore

echo "Done"

