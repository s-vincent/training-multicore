#!/bin/sh

set -e

sudo yum install ansible-core -y
sudo yum install ansible-collection-ansible-posix -y || echo
ansible-galaxy install -r  requirements.yml

ssh-keygen -t rsa -f ~/.ssh/id_rsa -N "" -q

