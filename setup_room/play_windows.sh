#!/bin/bash

ROOM_USER=Orsys

if [ -z "${ROOM_PASSWORD}" ]; then
  read -s -p "Enter password: " ROOM_PASSWORD
  export ROOM_PASSWORD
fi

#ansible-galaxy install -r requirement.yml
#pip3 install pypsexec --user --break-system-packages

ansible-playbook -i inventory/hosts_windows -u ${ROOM_USER} playbook_windows.yml "$@" -e ansible_password='{{ lookup("env", "ROOM_PASSWORD") }}'  -e ansible_become_pass='{{ lookup("env", "ROOM_PASSWORD") }}'

