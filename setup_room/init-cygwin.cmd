REM Cygwin setup

choco install cygwin -y
C:\tools\cygwin\cygwinsetup.exe -q --packages="binutils,gcc-core,gcc-g++,wget,python3,python3-devel,python3-winrm,python3-pip,libffi-devel,libgmp-devel,make,vim,python39-cryptography"

C:\tools\cygwin\bin\bash --login -c 'pip3 install ansible-core pywinrm pypsexec'
C:\tools\cygwin\bin\bash --login -c 'cd "%cd%" ; ansible-galaxy install -r requirements.yml'
C:\tools\cygwin\bin\bash --login -i -c 'cd "%cd%" ; exec /bin/bash'

