echo "Install some packages"
choco install -y git sysinternals
choco install -y visualstudio2022community visualstudio2022-workload-nativedesktop
choco install -y hwinfo opencl-intel-cpu-runtime

echo "Y" | winget source update
echo "Y" | winget install --id=microsoft.msmpisdk
echo "Y" | winget install --id=microsoft.msmpi

echo "Set registry to allow psexec to work in non-domain setup"
New-ItemProperty -Path "HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name LocalAccountTokenFilterPolicy -PropertyType DWord -Value 1 -Force

# adds Git directory to Path
$env:Path += ";C:\Program Files\Git\cmd"

$desktoppath = "C:\Users\vagrant\Desktop"
$env:GIT_REDIRECT_STDERR='2>$null'

echo "Install vcpkg"
if ((Test-Path -Path C:\tools\vcpkg) -eq $false) {
  git clone -b 2024.12.16 --depth 1 https://github.com/microsoft/vcpkg.git C:\tools\vcpkg 2>$null
}

if ((Test-Path -Path C:\tools\vcpkg\vcpkg.exe) -eq $false) {
  C:\tools\vcpkg\bootstrap-vcpkg.bat -disableMetrics

  echo "Compile OpenCL SDK"
  C:\tools\vcpkg\vcpkg.exe install opencl --triplet=x86-windows
  C:\tools\vcpkg\vcpkg.exe install opencl --triplet=x64-windows
  C:\tools\vcpkg\vcpkg.exe integrate install
}

if ((Test-Path -Path $desktoppath\clinfo) -eq $false) {
  git clone --depth 1 https://github.com/Oblomov/clinfo.git $desktoppath\clinfo 2>$null
}

echo "Compile clinfo"
if ((Test-Path -Path $desktoppath\clinfo\clinfo.exe) -eq $false) {
  pushd
  cd $desktoppath\clinfo
  Remove-Item -Path include -Recurse -Force -ErrorAction 'silentlycontinue'
  Remove-Item -Path lib -Recurse -Force -ErrorAction 'silentlycontinue'
  .\fetch-opencl-dev-win.cmd x64 2>$null
  cmd.exe /c '"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64 && .\make.cmd'
  copy clinfo.exe C:\Windows\System32\clinfo.exe
  popd
}

echo "Retrieves training-multicore git repo"
if ((Test-Path -Path $desktoppath\training-multicore) -eq $false) {
  git clone --depth 1 https://framagit.org/s-vincent/training-multicore.git $desktoppath\training-multicore 2>$null
}

echo "Retrieves hwloc binaries/libs"
$ProgressPreference = 'SilentlyContinue'
$hwlocuri = "https://download.open-mpi.org/release/hwloc/v2.11/hwloc-win64-build-2.11.2.zip"
Invoke-WebRequest -Uri $hwlocuri -OutFile "$desktoppath\$(Split-Path -path $hwlocuri -leaf)"
Expand-Archive -Path "$desktoppath\hwloc-win64*zip" -DestinationPath "$desktoppath\" -Force
Remove-Item -Path $desktoppath\hwloc-win64*zip

# Issue with git command, redirection 2> traces also in Error variable (should be fixed in Powershell 7.1.x)
$error.Clear()

echo "Finished"

Exit 0
