#Requires -RunAsAdministrator

$cidr = "192.168.1."
$computers_range = (1..10)
# non-contiguous
#$computers_range += 12
$trainer = $cidr + "100"
$room_user = $env:USERNAME
$packages = ("tightvnc", "hwinfo", "procexp", "opencl-intel-cpu-runtime")
$scriptdir = Split-Path -Parent $PSCommandPath
$desktoppath = "C:\Users\$room_user\Desktop"

echo "Setup computers for multicore training session"

echo "Install vcpkg"
if ((Test-Path -Path C:\tools\vcpkg) -eq $false) {
  git clone -b 2024.10.21 --depth 1 https://github.com/microsoft/vcpkg.git C:\tools\vcpkg
}

if ((Test-Path -Path C:\tools\vcpkg\vcpkg.exe) -eq $false) {
  C:\tools\vcpkg\bootstrap-vcpkg.bat -disableMetrics
}

echo "Compile OpenCL SDK"
C:\tools\vcpkg\vcpkg.exe install opencl --triplet=x64-windows
C:\tools\vcpkg\vcpkg.exe install opencl --triplet=x86-windows
C:\tools\vcpkg\vcpkg.exe integrate install

if ((Test-Path -Path $desktoppath\clinfo) -eq $false) {
  git clone --depth 1 https://github.com/Oblomov/clinfo.git $desktoppath\clinfo
}

echo "Compile clinfo"
if ((Test-Path -Path $desktoppath\clinfo\clinfo.exe) -eq $false) {
  pushd
  cd $desktoppath\clinfo
  .\fetch-opencl-dev-win.cmd x64
  cmd.exe /c '"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64 && .\make.cmd'
  popd
}

echo "Retrieves training-multicore git repo"
if ((Test-Path -Path $desktoppath\training-multicore) -eq $false) {
  git clone --depth 1 https://framagit.org/s-vincent/training-multicore.git $desktoppath\training-multicore
}

echo "Retrieves hwloc binaries/libs"
$ProgressPreference = 'SilentlyContinue'
$hwlocuri = "https://download.open-mpi.org/release/hwloc/v2.11/hwloc-win64-build-2.11.2.zip"
Invoke-WebRequest -Uri $hwlocuri -OutFile "$desktoppath\$(Split-Path -path $hwlocuri -leaf)"
Expand-Archive -Path "$desktoppath\hwloc-win64*zip" -DestinationPath "$desktoppath\"
Remove-Item -Path $desktoppath\hwloc-win64*zip

echo "Setup VNC configuration"
New-Item -Path "HKLM:Software\TightVNC\Server" -Force
New-ItemProperty -Path "HKLM:Software\TightVNC\Server" -Name EnableFileTransfers -PropertyType DWord -Value 0
New-ItemProperty -Path "HKLM:Software\TightVNC\Server" -Name RemoveWallpaper -PropertyType DWord -Value 0
New-ItemProperty -Path "HKLM:Software\TightVNC\Server" -Name PasswordViewOnly -PropertyType Binary -Value ([byte[]](0xA1,0xDD,0x7E,0xB2,0x0F,0x69,0x80,0xCF))
((Get-Content $scriptdir\files\student.vnc) -Replace 'PUT-IP-HERE', $trainer) |  Set-Content "$desktoppath\trainer.vnc"

echo "Install packages"
choco install -y cmder sysinternals vim $packages
winget install microsoft.msmpisdk microsoft.msmpi --silent --nowarn

$computers_range | ForEach {
    $addr = $cidr + $_

    If (Test-Connection $addr -Count 1 -Quiet){
        echo "Begin installation on $addr"
        $vnc = "student" + $_ + ".vnc"
        $smbaddr = "\\" + $addr
        $destination = $smbaddr\c$\Users\$room_user\Desktop\

        # VNC file for each student
        ((Get-Content $scriptdir\files\student.vnc) -Replace 'PUT-IP-HERE', $addr) |  Set-Content $desktoppath\$vnc

        # Copy stuff to remote computer
        Copy-Item -Path $scriptdir\files\caracteres.txt -Destination $destination
        Copy-Item -Path $desktoppath\training-multicore -Destination $destination -Recurse
        Copy-Item -Path $desktoppath\*.pdf -Destination $destination
        Copy-Item -Path $desktoppath\trainer.vnc -Destination $destination
        Copy-Item -Path $desktoppath\hwloc-win64* -Destination $destination -Recurse
        Copy-Item -Path $desktoppath\clinfo\clinfo.exe -Destination $smbaddr\c$\Windows\System32\
        Copy-Item -Path C:\tools\vcpkg -Destination $smbaddr\c$\tools\ -Recurse -Force

        # Integrate vcpkg installed packages in Visual Studio
        psexec $smbaddr C:\tools\vcpkg\vcpkg.exe integrate install

        # VNC configuration on remote computer
        psexec $smbaddr reg add "HKLM\Software\TightVNC\Server" /f /v "EnableFileTransfers" /t REG_DWORD /d 0
        psexec $smbaddr reg add "HKLM\Software\TightVNC\Server" /f /v "RemoveWallpaper" /t REG_DWORD /d 0
        psexec $smbaddr reg add "HKLM\Software\TightVNC\Server" /f /v "PasswordViewOnly" /t REG_BINARY /d A1DD7EB20F6980CF
        # install packages on remote computer
        psexec $smbaddr choco install -y $packages
        psexec $smbaddr cmd /c "winget install microsoft.msmpisdk microsoft.msmpi --silent --nowarn"

        echo "Done on $addr"
    }
}

Remove-Item -Path $desktoppath\trainer.vnc

