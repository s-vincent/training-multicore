# training-multicore

Multicore training files.

## Usages

This repository contains examples and Visual Studio project / Makefile for
multicore training course.

## License

All codes are under BSD-3 license.

