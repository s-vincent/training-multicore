# Vagrant setup

## Requirements

Install vagrant.

Install the following vagrant plugins:
```
vagrant plugin install winrm-elevated
```

## VM

in linux/ directory:
* alma01 (10.67.0.11): GNU/Linux Alma 9
* alma02 (10.67.0.12): GNU/Linux Alma 9
* deb01 (10.67.0.13): GNU/Linux Debian 12
* deb02 (10.67.0.14): GNU/Linux Debian 12

in windows/ directory:
* win01 (10.67.0.15): Windows 11 with Visual Studio 2022 Community
* win02 (10.67.0.16): Windows 11 with Visual Studio 2022 Community

## Start VM

Go the windows/ or linux/ directory then run:

```
vagrant up <name>
```

## Destroy VM

Go the windows/ or linux/ directory then run:

```
vagrant destroy <name> -f
```

## Connection

For GNU/Linux VM :

```
vagrant ssh <name>
```

For Windows VM :

```
vagrant rdp <name> -- /cert:ignore
```

