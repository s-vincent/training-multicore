# Sets french keyboard
echo "Set language to fr-FR"
# Use Start-Job because Set-WinUserLanguageList hangs on Windows 11...
Start-Job -ScriptBlock { Set-WinUserLanguageList -LanguageList "fr-FR" -Force }

# Enable Network share discovery
echo "Enable Network discovery"
netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes
netsh advfirewall firewall set rule group="File and Printer Sharing" new enable=Yes

echo "Install Chocolatey"
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

