#!/bin/bash

echo "Provisioning virtual machine..."

echo "Configure localtime"
if [ -z $TZ ]; then
  TZ="Europe/Paris"
fi

sudo rm /etc/localtime
sudo ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime

echo "Configure access to root account via public key"
sudo mkdir -p /root/.ssh
sudo cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

echo "Adds information to /etc/hosts"
cat <<EOF >> /etc/hosts
  10.67.0.11 alma01
  10.67.0.12 alma02
  10.67.0.13 debian01
  10.67.0.14 debian02
EOF

echo '& ~ # { } [ ] | ` \ ^ @ $ € < > !' > /home/vagrant/caracteres.txt

cat <<EOF >> /home/vagrant/.vimrc
set ts=2
set st=2
set sw=2
set expandtab
set cindent
set tabstop=2
set softtabstop=2
set autoindent
set smartindent
set smarttab
set encoding=utf-8
set fileencoding=utf-8
syntax on
set sta
set et
set ai
set si
set cin
if has("autocmd")
filetype plugin indent on
endif
EOF

echo "Bring back sshd with password"
sed -i 's/PasswordAuthentication.*/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd
echo "Set a password for vagrant user"
echo "vagrant:vagrant" | chpasswd

echo "End provisioning"

